///
/// @file ThriftTools_Unix.cpp
/// @brief Implementation of platform-dependent functions dealing with 
///        networking and thrift.
//------------------------------------------------------------------------------
// Copyright 2012
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

#include "ThriftTools.hpp"

#include <signal.h>

void imi::initializeThrift( void )
{
    // NOOP
}

void imi::ignoreSignals( void )
{
    // We don't want to handle SIGPIPE signals, but rather do error
    // handling at the time of socket.write, so we ignore.
    // Note signal handling is done per-thread
    signal(SIGPIPE, SIG_IGN);
    sigignore(SIGPIPE);
}