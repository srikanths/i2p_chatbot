\chapter{Concept, technologies and architecture}
\label{sec:architecture}

In this chapter we are presenting the overall concepts that motivate the creation of our common platform
along with the technologies chosen in order to complete this platform, cf.~\ref{sec:arch-concept}. Then 
we provide a small guide to go through installation, configuration and daily activities regarding the platform,
cf.~\ref{sec:arch-configuration}. Finally we give an overview of the current architecture, cf.~\ref{sec:arch-network}.
This overview has to goal: provide a better understanding of connection between modules and their relation with
computers, the second is to help users re-implement or modify the architecture if needed. 

\bigskip{}

{\noindent All information about login and password can be found on the I2P page of
\href{http://intranet.imi.ntu.edu.sg/wiki/index.php/I2P}{IMI wiki}.}

\section{Concept and technologies}
\label{sec:arch-concept}

The project was divided into some small projects/researches. Therefore, we wanted to
to make system modular which can combine all these scattered researches.
The main constraint is to find a way to do a smooth and robust integration. In our case
we have many researches involved, from different fields, using different software (C++,
python, matlab, java), which were not meant to be integrated.

In this section, we first present our choice to use a Remote Procedure Call (RPC) library
to handle the communication between modules. Then, we describe the development steps and 
architecture chosen for this platform. 

\subsection{RPC - Thrift}
\label{sec:RPC}

To solve the issue of integrating heterogeneous research modules a Remote Procedure Call (RPC)
library has been chosen. RPC allows inter-process communication and rely on a server/client
architecture. The advantage is that users don't have to worry about network implementation and 
they can focus on high-level interfaces.

\paragraph{Advantages.} RPC is \emph{modular}: it allows to keep high separation between research works and
eases the replacement of modules (using the same interface). RPC is \emph{flexible}:
it supports a large choice of platforms and programming languages and it has a low
research impediment (each team keeps its own environment and pace). So it gives
higher \emph{independence} for research teams and improve the \emph{maintainance}
and \emph{stability}. Finally the \emph{distributed architecture} allows us to share
the computation load among network nodes.

\paragraph{Drawbacks.} The \emph{scalability} of the network communication is unknown. 
The system is dependent on the network for delays as for the quantity of messages. The
\emph{dedication} of one team to supervise the integration and control the good
use of interfaces.

\paragraph{Thrift.} Among many choices, we picked thrift. It supports many platforms: Windows, Linux, OS X;
many languages: C\#, C++, Objective-C, Java, Python, SmallTalk, Ruby, Javascript, PHP. 
Objective-C and Java allows us to run it on smartphones and tablets.
Through Javascript or PHP, thrift applications could run on any device with an internet browser.


\subsection{Development tools}
\label{sec:devel-tools}

For greater maintainability, IMI enforces the use of development tools for every modules, cf. 
Figure~\ref{fig:devel-tools}:
\begin{my_itemize}
\item CMake: a cross-platform, open-source build system
\item Git: revision control and source code management software 
\item Web interface: management of remote repositories 
\end{my_itemize}

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.7\linewidth]{dt-devel-tools.pdf}
	\caption[Tools for development]{Illustration of the different components of the code managing system.
	Access, retrieving and installation of modules.}
	\label{fig:devel-tools}
\end{figure}


\paragraph{CMake} is an application that allows us to have a better management of the 3\textsuperscript{rd}
party libraries. Using a configuration file it will look for required libraries on the system and
generate a solution. This multi-platform tool enables easy installation and deployment of projects (Windows,
Unix, Mac OS).

\paragraph{Git} is a tool to perform code tracking (it is similar to older CVS and SVN systems). It
allows collaborative work, tracking of changes. The default behavior is to duplicate the code
on multiple places and thus provide a protection against data loss.

\paragraph{Bitbucket} is a web service used to host repositories. We use Bitbucket as it supports
git and it provides free accounts for academia. It is an external service for storage and
therefore eases the access to external people. The web based interfaces is a user friendly tool
to manage the repositories. It also supports many other features, e.g. wiki and documentation.



\section{Configuration and Installation of modules}
\label{sec:arch-configuration}

This section describes the usual steps to go through in order 1) to set-up a computer be `I2P ready', 
2) to install, compile and run an I2P module and 3) to create a new module.

\subsection{Setting the computer}
This is a one time action to be perform on any computer where you want to install an I2P module.

\subsubsection{Installing external programs}
\label{sec:concept-external-programs}

Before installing any of these software make sure they are not already available in your system. For
instance Visual Studio is part of the default installation for every IMI computer.

\paragraph{C++}
Install a recent visual studio solution for windows or any GCC compiler on Linux.
Boost library is mandatory, for Windows it is packaged on one of IMI server cf. sec.~\ref{sec:I2P-dependencies}.

\paragraph{Python}
All our code was run on Python 2.7 version(32bit), however it is possible that Python 2.6
might also be necessary if you plan to run the module for virtual human sec.~\ref{sec:module:control-virtualhuman}
that relies on Smartbody.
Usual libraries for python includes: numpy, scipy, thrift, matplotlib and PyQt (VH test client). These libraries
can be downloaded from Internet or installed using python `easy-install' script.

\paragraph{Java}
Currently, it is exclusively used by the robokind module. Please install a decent Java IDE such as netbeans.
Make sure the `maven' pluggin is installed with your IDE.

\paragraph{Git}
This is our software management application, we use it to maintain and share our repositories.
\href{http://msysgit.github.com/}{Git for Windows}. During installation we recommend we use the
following settings: `use Git bash only' and `checkout as-is, commit unix style'.


\paragraph{Bitbucket}
\href{https://bitbucket.org/dashboard/overview}{Bitbucket} is the online service hosting all our repositories.
Compare to svn, git does not centralize all repositories, so if Bitbucket ceases its activities we can
reset a new common server on another website or internally. Bitbucket allows connection through https or
ssh. The later is highly recommended as it remove the hindrance of retyping ones password at every git command.
Please check \href{https://confluence.atlassian.com/display/BITBUCKET/How+to+install+a+public+key+on+your+bitbucket+account}{here} and \href{https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git}{here} for more information. In order
to create a filename such as .bashrc on windows you can use the command `touch .bashrc' from Git environment.
Use of Bitbucket:
\begin{my_itemize}
\item getting an account: go on the website and create and account;
\item get access to I2P: ask one IMI staff to log on Bitbucket and add you as a user;
\item adding a user: log on Bitbucket, in the global tab about your profile select `imintu' under `Team'. Now
you can administrate the account: add/remove users, add/remove repositories;
\item adding repository: log on Bitbucket, change to `imintu' (cf. previous bullet), add a new repository;
\item normal usage: please do all other actions from your account and not on behalf of `imintu'.
\end{my_itemize}

\paragraph{CMake}
\href{http://www.cmake.org/}{CMake} is a multi-platform application that allows to prepare projects from source code.
We use it to: find automatically libraries, generate C++ solution (visual or makefiles), 
set-up environment, copy data files.

\subsubsection{Setting up the environment and Downloading I2P resources}
\label{sec:I2P-dependencies}

\paragraph{Windows}
\begin{my_itemize}
\item Create a folder to hold libraries for I2P (libraries can take up to 6 GB);
\item Add an environment variable `IMI\_LIBRARIES' pointing to this folder;
\item Access `\textbackslash{}\textbackslash{}imi-server\textbackslash{}Public\textbackslash{}Tools\textbackslash{}IMI-Libraries' folder. Copy and extract `IMI-Win32Binaries.7z'
in your local folder. Add the path to this new created folder in your system path: update the `PATH' environment variable;
\item Access `\textbackslash{}\textbackslash{}imi-server\textbackslash{}Public\textbackslash{}Tools\textbackslash{}IMI-Libraries\textbackslash{}package' folder. Copy and extract `boost\_1\_49\_0.7z' and `others.7z'
in your local folder. Do the same with `OpenCV-BlobLib.7z' for modules requiring vision, `smartbody.7z' and `VH\_SpeechRelay.7z'
for vh control module;
\end{my_itemize}

\paragraph{Linux/Mac}
Please download necessary development tools or libraries from your package manager.

\subsection{Getting a module running}

\subsubsection{Download}
\label{sec:concept-download}
Clone the module on the computer. You can find the command on the Bitbucket main webpage of your module.
Then make sure the submodule I2P has been download/configured properly as explained \href{https://bitbucket.org/imintu/i2p/wiki/Home}{here}
\begin{lstlisting}
git clone git@bitbucket.org:imintu/NAME_OF_MODULE.git

cd NAME_OF_MODULE
git submodule init
git submodule update
cd i2p
git checkout master
\end{lstlisting}

\subsubsection{Generate and configure your module}
\label{sec:concept-cmake}

\paragraph{Windows}
Run CMake-gui, as source code type the path of the newly download repository `path\_to\_repository' and for build path
type `path\_to\_repository\textbackslash{}build'.
Then press `configure' and choose a proper compiler such as a recent visual studio. Then `generate' if there were no error during configure step.
In this two steps CMake will include header files, link libraries, copy files, generate thrift files, generate solution/makefile as specified in CMake file of module.

\paragraph{Linux}
In a terminal console in the root of the recently downloaded repository, type:
\begin{lstlisting}
mkdir build
cd build

cmake ..
\end{lstlisting}

\subsubsection{Compile and run your module}
Depending on the programing language and environment you'll do different things:
\begin{my_itemize}
\item Python: just run the scripts;
\item C++: compile using Visual Studio or the makefile. Running is usually done from Visual;
\item Java: use your favorite IDE.
\end{my_itemize}


\subsection{Creating a new module}
The best solution in order to create a new module is to copy from existing modules. Look
for a module that share most characteristic with the module you want to create:
python-based or C++ based, libraries used, OS used, Thrift services used. Then look
at the way the `CMakeLists.txt' is written.
Basic steps are:
\begin{my_itemize}
\item Creating a repository: directly on Bitbucket or imported from local;
\item Create a CMakeLists.txt based on example to generate automatically your solution;
\item Testing;
\end{my_itemize}

\subsection{Updating a new module}
\todo{A small guideline should be done on this topic}

\section{Network architecture}
\label{sec:arch-network}

In this section we will describe the current set-up of the system, \emph{3 March 2013}, in respect to:
\begin{my_itemize}
\item real location of computer;
\item computer configuration;
\item current used for those computers in terms of modules;
\item network connection and set-up.
\end{my_itemize}

\subsection{Computers}

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\linewidth]{NetworkConfiguration.pdf}
	\caption[Network configuration]{Main computers and wireless devices used to run I2P and for demonstration.
	Note that there are two separate networks: one is NTU network, the other is a local network for the robots
	and some computers such as PC3 and PC4.}
	\label{fig:Network-configuration}
\end{figure}

Figure~\ref{fig:Network-configuration} shows the main components used to perform demonstration inside
IMI. There are 4 main computers and it is easy to plug a new laptop or use another computer of IMI and
to add it to the current diagram. The robots benefit from a separate wireless network that they share with 
two of the main computers.

\paragraph{PC 1 - WorldModel}
Alienware computer mainly used under Linux but has a Windows7 dual boot. The main modules we run on this
computer are the world model cf. sec.~\ref{sec:module:worldmodel} and the object vision localization
cf. sec.~\ref{sec:module:inputs-objectvisionlocalization}. This computer is connected to the big
screen, one Kinect and NTU wired network.

\paragraph{PC 2 - General}
Alienware computer used under Windows7. This computer is used to run almost all the modules needed for a demo:
face recognition cf. sec.~\ref{sec:module:inputs-facereco},
hand recognition cf. sec.~\ref{sec:module:inputs-handgesture},
object vision localization cf. sec.~\ref{sec:module:inputs-objectvisionlocalization},
sound localization cf. sec.~\ref{sec:module:inputs-kinectsoundlocal},
sound processing cf. sec.~\ref{sec:module:inputs-micarraysoundproc},
world model cf. sec.~\ref{sec:module:worldmodel} and
VH control cf. sec.~\ref{sec:module:control-virtualhuman}.
This computer is connected to the big screen, one Kinect, one webcam and NTU wired network.

\paragraph{PC 3 - Robots}
HP Compaq computer used under Linux. This computer is on the same network than the robots' Wifi
so we can easily access them. Both robots run Linux OS.
The main modules we run on this are: Nao driver cf. sec.~\ref{sec:module:control-nao},
the robokind driver cf. sec.~\ref{sec:module:control-robokind},
and the social network module cf. sec.~\ref{sec:module:socialnetwork}.
This computer has the API of Nao and Robokind installed and is used to troubleshot those two robots.
This computer is connected to the router and can be used to change router configuration.

\paragraph{PC 4 - Gesture reco}
Acer computer used Windows7. Mainly used for the gesture recognition 
cf. sec.~\ref{sec:module:inputs-kinectusertracking}.
This computer is connected to the router and can be used to change router configuration.

\paragraph{Other noticeable PC}
The Thinkpad laptop running windows XP is used to run Choregraphe: Gui interface to Nao.

\subsection{Network configuration}

All the information about the port numbers used by thrift interfaces can be found
in the thrift files defining the services.

The two computers, PC3 and PC4, can access to NTU network so there is no special configuration for 
modules leaving robot network to reach other computers. However the computers on the NTU network
cannot see the internal network and thus needs to go through the router.

\paragraph{Configuration}
Figure~\ref{fig:Router-config} is an example of configuration for the router. In order to access
the configuration frame please connect to the router using the ip 192.168.1.1 (local network)
or 155.69.52.10 on the NTU network. Once you logged in, cf. \href{http://intranet.imi.ntu.edu.sg/wiki/index.php/I2P}{IMI wiki},
go to the `Applications \& Gaming' tab. Here it is possible to define redirections from the router to 
the appropriate computer (per port number). 
Example: PC3 must run a Nao Controller server that
will be accessed from the decision making program running on PC1:
\begin{my_itemize}
\item PC3, Nao controller server: ip is `localhost' or the current ip of the current PC3 (192.168.1.107),
port is the one defined for Nao control (10013).
\item PC1, application client: ip is not the ip of PC3 that is a local address but the one from the router,
e.g. 155.69.52.10, port is still 10013.
\item Router: we redirect message for Nao control (port 10013) to PC3 (local address 192.168.1.107).
\end{my_itemize}


\begin{figure}[htp]
	\centering
	\includegraphics[width=0.7\linewidth]{Router-Config.png}
	\caption[Router configuration]{Port redirection used to let modules run on external computers access servers
	on internal computers.}
	\label{fig:Router-config}
\end{figure}
