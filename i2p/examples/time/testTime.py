#How to get time stamp in python
#It is time since epoch express in i64 and represents micro-seconds
import time
from threading import Thread
import numpy

def getTimeStampMicro():
    return numpy.int64(time.time()*1000000)
    

class testTime(Thread):
    def run(self):
        for x in range(0,10):
                print 'time in seconds since epoch: ' + str(time.time())
                print 'time in microseconds since epoch: ' + str(time.time()*1000000)
                #a numpy integer 64
                print 'numpy: ' + str(getTimeStampMicro())
                print ''
                #under windows precision is not fantastic (a 100th of a second)
                time.sleep(0.2)
            

testTime().start()