if (WIN32)
  FIND_PACKAGE(Thrift REQUIRED)
  SET (THRIFT_COMPILER ${THRIFT_DIR}/thrift.exe)
endif()

if (UNIX AND NOT APPLE)
    #
    # FIND_PKG_MODULE PREFIX LIBNAME
    # Looks for the library with libname in the the library paths
    # in the PKG_SEARCH_MODULE results
    #
    # Creates variables:
    #  ${PREFIX}_FULLPATH_LIBRARIES - the full paths of the libraries to link
    #  ${PREFIX}_INCLUDE_DIRS - the include dirs, which are already included
    #
    MACRO (FIND_PKG_MODULE PREFIX LIBNAME)
        if(${PREFIX}_FOUND)
            foreach(lib ${${PREFIX}_LIBRARIES})
                find_library(${lib}_LIBRARY
                    NAMES ${lib}
                    HINTS ${${PREFIX}_LIBRARY_DIRS}
                )
                if(${lib}_LIBRARY)
                    list(APPEND ${PREFIX}_FULLPATH_LIBRARIES "${${lib}_LIBRARY}")
                endif(${lib}_LIBRARY)
            endforeach(lib)
       endif(${PREFIX}_FOUND)
    ENDMACRO(FIND_PKG_MODULE)
    
    
  FIND_PACKAGE(PkgConfig)
  PKG_SEARCH_MODULE(THRIFTprefix REQUIRED thrift)
  FIND_PKG_MODULE(THRIFTprefix thrift)
  SET (THRIFT_COMPILER thrift)
  LIST(APPEND I2P_INCLUDES "${THRIFTprefix_INCLUDE_DIRS}")
  LIST(APPEND I2P_LIBS "${THRIFTprefix_FULLPATH_LIBRARIES}")
  
  PKG_SEARCH_MODULE(EVENTprefix REQUIRED libevent)
  FIND_PKG_MODULE(EVENTprefix libevent)
  LIST(APPEND I2P_INCLUDES "${EVENTprefix_INCLUDE_DIRS}")
  LIST(APPEND I2P_LIBS "${EVENTprefix_FULLPATH_LIBRARIES}")

  MESSAGE( STATUS "I2P_INCLUDES: ${I2P_INCLUDES}")
  MESSAGE( STATUS "I2P_LIBS: ${I2P_LIBS}")
endif(UNIX AND NOT APPLE)
if(APPLE)
  SET (THRIFT_COMPILER thrift)
endif(APPLE)

MACRO (GENERATE_STUBS THRIFT_FILE LANGUAGE)
    execute_process(COMMAND  ${THRIFT_COMPILER} -o  ${CMAKE_SOURCE_DIR} --gen  ${LANGUAGE}   ${CMAKE_SOURCE_DIR}/i2p/ServiceDefinitions/${THRIFT_FILE})
ENDMACRO(GENERATE_STUBS)
