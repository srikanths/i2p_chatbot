namespace cpp imi
namespace java imi
namespace php imi

const i32 DEFAULT_FACEBOOK_SERVICE_PORT = 14000;
const i32 DEFAULT_GOOGLE_SERVICE_PORT = 14001;

# the account accessed is selected by the user when starting any of this service

service Facebook
{
    oneway void postMessage( 1:string message )
                             
    #the binary is a jpeg file
    oneway void postMessageAndPicture( 1:string message,
                                       2:binary picture)
}

service Google
{
    #return one event
    map<string,string> calendar_getEventNow( 1:i32 eventID)
    
    #return all events
    list< map<string,string> > calendar_getEventsNow()
}
