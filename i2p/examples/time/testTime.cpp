#include <iostream>
#include <fstream>
#include <sstream>

#include <boost/date_time/local_time/local_time.hpp>

static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));


int64_t getTimeStamp()
{
    return (boost::posix_time::microsec_clock::universal_time() - epoch).total_microseconds();
}


int main(int argc, char* argv[])
{
    int i = 0;
    while( i<100)
    {
        std::cout << "time stamp in microseconds " << getTimeStamp() << std::endl;
        i++;
    }
}