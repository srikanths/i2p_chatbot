###############################################################################
# Find KinectSDK
#
# This sets the following variables:
# KinectSDK_FOUND - True if KinectSDK was found.
# KinectSDK_INCLUDE_DIRS - Directories containing the KinectSDK include files.
# KinectSDK_LIBRARIES - Libraries needed to use KinectSDK.
# KinectSDK_DEFINITIONS - Compiler flags for KinectSDK.KinectSDK

set(KinectSDK_DEFINITIONS ${PC_KinectSDK_CFLAGS_OTHER})

#using the 64bit version of KinectSDK if generating for 64bit
#if(CMAKE_SIZEOF_VOID_P EQUAL 8)
#    set(PROGRAMFILES_ "$ENV{PROGRAMW6432}")
#    set(KinectSDK_SUFFIX "64")
#else(CMAKE_SIZEOF_VOID_P EQUAL 8)
#    set(PROGRAMFILES_ "$ENV{PROGRAMFILES}")
#    set(KinectSDK_SUFFIX "")
#endif(CMAKE_SIZEOF_VOID_P EQUAL 8)

# OVERRIDE to use the Program Files env dir
set(PROGRAMFILES_ "$ENV{PROGRAMFILES}")
message(STATUS "Program Files ${PROGRAMFILES_}")
#Hopefully KINECTSDK10_DIR is set up by kinect installer

#add a hint so that it can find it without the pkg-config
find_path(KinectSDK_INCLUDE_DIR NuiApi.h
    HINTS ${PC_KinectSDK_INCLUDEDIR} ${PC_KinectSDK_INCLUDE_DIRS}
	"$ENV{KINECTSDK10_DIR}/inc"
    "${PROGRAMFILES_}/Microsoft SDKs/Kinect/v1.5/inc"
    "${PROGRAMFILES_}/Microsoft SDKs/Kinect/v1.6/inc"
    PATH_SUFFIXES kinectsdk)
#add a hint so that it can find it without the pkg-config
find_library(KinectSDK_LIBRARY 
    NAMES Kinect10.lib
    HINTS ${PC_KinectSDK_LIBDIR} ${PC_KinectSDK_LIBRARY_DIRS} "$ENV{KINECTSDK10_DIR}/lib/x86" "${PROGRAMFILES_}/Microsoft SDKs/Kinect/v1.5/lib/x86"
    HINTS ${PC_KinectSDK_LIBDIR} ${PC_KinectSDK_LIBRARY_DIRS} "$ENV{KINECTSDK10_DIR}/lib/x86" "${PROGRAMFILES_}/Microsoft SDKs/Kinect/v1.6/lib/x86")

set(KinectSDK_INCLUDE_DIRS ${KinectSDK_INCLUDE_DIR})
set(KinectSDK_LIBRARIES ${KinectSDK_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(KinectSDK DEFAULT_MSG
    KinectSDK_LIBRARY KinectSDK_INCLUDE_DIR)
    
mark_as_advanced(KinectSDK_LIBRARY KinectSDK_INCLUDE_DIR)
if(KinectSDK_FOUND)
  include_directories(${KinectSDK_INCLUDE_DIRS})
  message(STATUS "KinectSDK found (include: ${KinectSDK_INCLUDE_DIR}, lib: ${KinectSDK_LIBRARY})")
endif(KinectSDK_FOUND)