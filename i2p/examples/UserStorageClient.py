import sys
sys.path.append("gen-py")

import UserStorage.UserStorage as us
from UserStorage.ttypes import *

from thrift.transport import TTransport
from thrift.transport import TSocket
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TCompactProtocol

 # Make an object
up = us.UserProfile(uid=1,
       name="Test User",
       blurb="Thrift is great")

# Talk to a server via TCP sockets, using a binary protocol
#Python doc explains that python socket (used by TSockect) will resolve the
#host num depending on dns or local configuration of eth
#therefore you do not know if it will be ipv6 or ipv4
#only way to be sure is to force ip address
transport = TSocket.TSocket('localhost', 9090)
#transport = TSocket.TSocket('ip address of server', 9090)

# Buffering is critical. Raw sockets are very slow
transport = TTransport.TBufferedTransport(transport)

# Wrap in a protocol
protocol = TBinaryProtocol.TBinaryProtocol(transport)

# Connect!
transport.open()

# Use the service we already defined
service = us.Client(protocol)
service.store(up)

# Retrieve something as well
up2 = service.retrieve(1)
print up2