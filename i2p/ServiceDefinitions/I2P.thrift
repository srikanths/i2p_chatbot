
namespace cpp imi
namespace java imi
namespace php imi

typedef i64 Microseconds
typedef i64 Sequence

struct Vec2
{
    1: double x,
    2: double y
}

# In general, Z-up
struct Vec3
{
    1: double x,
    2: double y,
    3: double z
}

struct Vec4
{
    1: double x,
    2: double y,
    3: double z,
    4: double w
}

typedef Vec4 Quaternion

# Position of each body segments
struct Skeleton
{
    1: Vec3 hipc,
    2: Vec3 spine,
    3: Vec3 shoulderc,
    4: Vec3 head,
    5: Vec3 shoulderl,
    6: Vec3 elbowl,
    7: Vec3 wristl,
    8: Vec3 handl,
    9: Vec3 shoulderr,
    10: Vec3 elbowr,
    11: Vec3 wristr,
    12: Vec3 handr,
    13: Vec3 hipl,
    14: Vec3 kneel,
    15: Vec3 anklel,
    16: Vec3 footl,
    17: Vec3 hipr,
    18: Vec3 kneer,
    19: Vec3 ankler,
    20: Vec3 footr
}

# We use same emotion for inputs of face recognition than for AI system and agent control
enum Emotion
{
    HOPE,
    FEAR,
    SATISFACTION,
    FEAR_CONFORMED,
    DISAPPOINTMENT,
    RELIEF,
    JOY,
    DISTRESS,
    GRATIFICATION,
    REMORSE,
    PRIDE,
    SHAME
}

# We differentiate the use of face expressions from emotions
# emotions are for the behavior and personnality models of VH and robot
# while face expressions are only for low level control of VH and robot
# based on 6 facial expressions from ekman 82
enum Facial_Expression
{
    NEUTRAL,
    ANGER,
    DISGUST,
    FEAR,
    HAPPINESS,
    SADNESS,
    SURPRISE
}

# A position in the World-Space coordinate frame, Z-up
typedef Vec3   WorldPosition

##########################################################################
# Names
# 
# A number of the APIs use strings to identify particular locations, people,
# or objects in the world.  There are several types for these strings, below.
#
# Every Name in the system should be unique, the types below are for 
# guidance for the API callers.
#
# A TargetName is a string that corresponds to the name of a known 
# object or actor in the world.
typedef string TargetName

# An ObjectName is a string that identifies some known object in the
# world.
typedef string ObjectName

# A PersonName is a string that identifies a particular person 
# in the world.  
# Note that PersonName \in TargetName, so all PersonNames 
# are valid TargetNames, but not all TargetNames are valid PersonNames.
typedef string PersonName


