///
/// @file ThriftTools.cpp
/// @brief Common library tools for all i2p projects.
///        This file holds only non-os-specific implementations,
///        Os-specific implementations should be os-labeled cpp files.
///
//------------------------------------------------------------------------------
// Copyright 2012
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

#include "ThriftTools.hpp"

#include <boost/date_time/local_time/local_time.hpp>
//defining Epoch
static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

#include <iostream>


int64_t imi::getTimeStamp()
{
    //Windows provide microseconds but I doubt they are reliable.
    return (boost::posix_time::microsec_clock::universal_time() - epoch).total_microseconds();
}

std::string imi::timeLogFilename( const std::string& baseFilename )
{
    std::string out = baseFilename;
    return out + ".timeStampLog";
}

std::string imi::eventLogFilename( const std::string& baseFilename )
{
    std::string out = baseFilename;
    return out + ".eventLog";
}
