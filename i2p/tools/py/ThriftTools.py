from thrift.transport import TTransport
from thrift.transport import TSocket
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

#thread
import threading
import time

#our servers
class ThriftServerThread(threading.Thread):
    #Service is a class name
    def __init__(self, port, Service, handler, name, host=''):
        threading.Thread.__init__(self)
        self.name = name
        processor = Service.Processor(handler)
        if (host == ''):
            transport = TSocket.TServerSocket(port=port)
        else:
            transport = TSocket.TServerSocket(host=host,port=port)
        tfactory = TTransport.TBufferedTransportFactory()
        pfactory = TBinaryProtocol.TBinaryProtocolFactory()
        #~ server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
        self.server = TServer.TThreadedServer(processor, transport, tfactory, pfactory)
        
    def run(self):
        print "Starting " + self.name
        self.server.serve()
        
    def stop(self):
        #waiting for python to have a stop function on servers
        #self.server.stop()
        return
        
#our clients
class ThriftClient():
    #Service is a class name
    def __init__(self, server, port, Service, name):
        self.name = name
        self.server = server
        self.port = port
        self.Service = Service
        self.connected = False
        
    def connect(self):
        if self.connected:
            return
        try:
            socket = TSocket.TSocket(self.server, self.port)
            self.transport = TTransport.TBufferedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocolFactory().getProtocol(self.transport)
            self.transport.open()
            self.client = self.Service.Client(protocol)
            self.connected = True
            print 'connected to ' + self.name + ' service:',self.server,self.port
        except TSocket.TTransportException:
            print 'can not connect to ' + self.name + ' service:',self.server
            raise
            # return
    
    def disconnect(self):
        if self.connected:
            self.transport.close()
            self.connected = False
