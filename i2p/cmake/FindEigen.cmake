# - Try to find Eigen
# Once done this will define
#  EIGEN_FOUND - System has Eigen
#  EIGEN_INCLUDE_DIRS - The Eigen include directories

find_path(EIGEN_INCLUDE_DIR Eigen/Eigen
    HINTS $ENV{IMI_LIBRARIES}/eigen)

set(EIGEN_INCLUDE_DIRS ${EIGEN_INCLUDE_DIR} )


include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set THRIFT_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Eigen  DEFAULT_MSG EIGEN_INCLUDE_DIR)

mark_as_advanced(EIGEN_INCLUDE_DIR)
