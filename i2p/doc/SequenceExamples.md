# Example Sequences for I2P #

## New Participant Sequence ##
AI->WM:  subscribe( '127.0.0.1', DEFAULT_AI_PORT, 'participantList' ) 
SS->WM:  newPersonDetected( 'unknown1' )
WM->AI:  participantAddedEvent( {'unknown1', {1,2,0.5}} )
AI->AC:  lookAt( 'unknown1' )
AC->CQ:  getLocationOfParticipant( 'unknown1' )
AC->CQ:  getLocationOfParticipant( 'unknown1' )

## Participant Named Sequence ##
SS->WM:  personNamed( {1.3,2,0.4}, 'Flavien' ) 
WM->AI:  participantRenamedEvent( {'unknown1', {1,2,0.5}}, 'Flavien' )
AI->AC:  lookAt( 'Flavien' )
AC->CQ:  getLocationOfParticipant( 'Flavien' )
AI->AC:  speak( 'Hello, Flavien' )

## Participant Speaking ##
AI->WM:  subscribe( '127.0.0.1', DEFAULT_AI_PORT, 'speech' ) 
SS->WM:  sound( 1, {1.0, 2.0, 0.5} )
SS->WM:  sound( 2, {1.1, 2.0, 0.6} )
WM->AI:  speechStartEvent( {'unknown1', {1.05, 2.0, 0.55}} )
AI->AC:  lookAt( 'unknown1' )
AC->CQ:  getLocationOfParticipant( 'unknown1' )
AC->CQ:  getLocationOfParticipant( 'unknown1' )
SS->WM:  sentencedRecognized( 'hello' )
WM->AI:  speechRecognizedEvent( {'unknown1', {1.05, 2.0, 0.55}}, 'hello' )
AI->AC:  speak( 'Hello there, what's your name?' )
