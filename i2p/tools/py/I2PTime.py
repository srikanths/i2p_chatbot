# Copyright Nanyang Technological University, Singapore, 2012
#
# Authors: 
#

import time, numpy

def getTimeStamp():
    return numpy.int64(time.time()*1000000)
