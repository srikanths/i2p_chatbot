# Integrated Interaction Platform

This repository contains the core information about the structure of the
I2P platform.
This module is usually integrated as a submodule in every repository
of modules relying on thrift. The goal is to ensure that the service definitions
and some common tools to ease creation of client/server are share uniformely by
every modules.
Therefore everytime i2p module is updated you should not forget to push it in
bitbucket and update all the other modules relying on the updated code.

## Service Definitions

The source folder 'Service Definitions' contains all the thrift interfaces necessary
for running the I2P modules: services, structures and functions. 

* I2P.thrift contains the general structures and types used in the other interfaces.
* Inputs.thrift contains a list of several services all related to inputs events.
* Control.thrift is for the control of the virtual agents: Virtual Human and social robots
* EventPublisher.thrift is aiming at defining intermediary events between fusion stage
receiveing inputs events) and decision making, use sparsely so far. Also contains
the function service allowing virtual agent to query world model about objects position
and orientation
* SocialNetwork.thrift is two very simple interface to access some data from facebook
and google calendar
* Devices.thrift is two external services: one to request picture from a device, here
Nao and the second to send some expected sentence to be recognized by the speech
recognition system and is not in use at the moment.

## Documentation
The folder 'doc' contains different version of the architecture and a documentation
about the full system: function calls and descritption of modules and applications.
However it has to be updated frequently.

## Tools
In order to facilitate the developpment of modules and applications this modules
also contains some common classes and functions:

### C++
In the folder src:

* I2PTime: getTimeStamp in microsecond
* ThriftTools: contains generic classes to wrap creation of thrift server or client
* transformations: some math functions on vector, matrix and quaternions

### Python
In the folder tools/py:

* ProtectedClient: client that should be able to reconnect automatically if server crashes.
It is a wrapper for a thrift client.
* ThriftTools: contains function to ease creation of thrift server and function to
get time stamp in microsecond
* ThrifTools_*: platform dependend code for initialisation of thrift

## Dependencies
This module is used as a submodule. It only provides common functions for 
C++ and Python. Its use will require to have thrift install on the computer.

* Thrift