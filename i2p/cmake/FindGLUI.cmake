# Locate the glui library
# This module defines the following variables:
# GLUI_LIBRARY, the name of the library;
# GLUI_INCLUDE_DIR, where to find glui include files.
# GLUI_FOUND, true if both the GLUI_LIBRARY and GLUI_INCLUDE_DIR have been found.
#
# To help locate the library and include file, you could define an environment variable called
# GLUI_ROOT which points to the root of the glui library installation. This is pretty useful
# on a Windows platform.
#
#
# Usage example to compile an "executable" target to the glui library:
#
# FIND_PACKAGE (glui REQUIRED)
# INCLUDE_DIRECTORIES (${GLUI_INCLUDE_DIR})
# ADD_EXECUTABLE (executable ${EXECUTABLE_SRCS})
# TARGET_LINK_LIBRARIES (executable ${GLUI_LIBRARY_RELEASE})
#
# TODO:
# Allow the user to select to link to a shared library or to a static library.

MESSAGE(STATUS "Looking for GLUI...")
#Search for the include file...
FIND_PATH(GLUI_INCLUDE_DIR GL/glui.h DOC "Path to GLUI include directory."
  HINTS $ENV{IMI_LIBRARIES}
  PATH_SUFFIXES glui/include
)
#MESSAGE(STATUS "GLUI INCLUDE PATH IS ${GLUI_INCLUDE_DIR}")

FIND_LIBRARY(GLUI_LIBRARY_DEBUG DOC "Absolute path to GLUI library."
  NAMES glui glui32_d.lib
  HINTS $ENV{IMI_LIBRARIES}
  PATH_SUFFIXES glui/lib/
)

FIND_LIBRARY(GLUI_LIBRARY_RELEASE DOC "Absolute path to GLUI library."
  NAMES glui glui32.lib
  HINTS $ENV{IMI_LIBRARIES}
  PATH_SUFFIXES glui/lib/
)

#MESSAGE(STATUS "GLUI LIBRARY PATH IS ${GLUI_LIBRARY}")

SET(GLUI_FOUND 0)
IF(GLUI_LIBRARY_DEBUG AND GLUI_LIBRARY_RELEASE AND GLUI_INCLUDE_DIR)
  SET(GLUI_FOUND 1)
  message(STATUS "GLUI found!")
ENDIF(GLUI_LIBRARY_DEBUG AND GLUI_LIBRARY_RELEASE AND GLUI_INCLUDE_DIR)