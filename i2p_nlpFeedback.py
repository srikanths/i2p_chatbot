import speech_recognition as sr
import pyaudio
import speech
import time
import aiml
import sys
import thread
#sys.path.append("../")
sys.path.append("gen-py")
sys.path.append("i2p/tools/py")

import Inputs.SpeechRecognitionService as SR_Service
from I2P.ttypes import *
from thrift.transport import TSocket
#to ease client server creation in python
import ThriftTools
import Inputs.constants
from Inputs.ttypes import *
import I2PTime

#For creating a feedback server
import Inputs.SpeechRecognitionService as Feedback_Service



runLoop='false'
k = aiml.Kernel()
k.learn("std-startup.xml")
k.respond("load aiml b")
transc=""
sentenceRecognized=''
tes=''

r=sr.Recognizer()

def recordAfterFeedback():
    global runLoop
    while(1):
        if(runLoop==1):
            print("Start Recording")
            with sr.Microphone() as source:
                audio = r.listen(source)
            print("ended listening")
            try:
                #print("Transcription: " + r.recognize(audio))
                transc=r.recognize(audio)
                if(runLoop==1):
                    sentenceRecognized=resp(transc)
                    print("Robot: "+ sentenceRecognized)
                    sr_client.client.sentenceRecognized('Microphone', I2PTime.getTimeStamp(), sentenceRecognized, 0.0)
                    sentenceRecognized=''
            except LookupError:
                print("Could not recognize the speech")



def resp(transc):
    sentenceRecognized=k.respond(transc)
    return sentenceRecognized

class SpeechRecoHandler:
    def __init__(self):
        return
        
    def __del__(self):
        return
        
    def sentenceRecognized(self, sensorID, timestamp, sentenceRecognized, confidence):
        print sentenceRecognized
        print('OUT')
        global runLoop
        if(runLoop!=0):
            runLoop=0
        if(runLoop==0):
            runLoop=1
            
        



"""def recordSpeech():
    #sr_client = ThriftTools.ThriftClient('155.69.52.73', Inputs.constants.DEFAULT_SPEECHRECOGNITION_SERVICE_PORT, SR_Service, 'Speech Recognition')
    sr_client = ThriftTools.ThriftClient('localhost', Inputs.constants.DEFAULT_SPEECHRECOGNITION_SERVICE_PORT, SR_Service, 'Speech Recognition')
    feedbackNadine_handler = FeedbackHandler()
    feedbackNadine_server = ThriftTools.ThriftServerThread(12010,Feedback_Service, speechReco_handler,'Feedback server','localhost')
    feedbackNadine_server.start()
    while True:
        try:
            print("AAAAAAAAAAAA")
            sr_client.connect()
            #break
        except TSocket.TTransportException:
            print 'can not connect to Speech Recognition service'
            time.sleep(1)
        except:
            break
        print("TTTTTTTTTTTTTTT")
        while True:
            print("Start Recording")
            #pdb.set_trace()
            with sr.Microphone() as source:
                    audio = r.listen(source)
                    print("ended listening")
                    try:
                        print("Transcription: " + r.recognize(audio))
                        transc=r.recognize(audio)
                        sentenceRecognized=resp(transc)
                        print("Robot: "+ sentenceRecognized)
                        sr_client.client.sentenceRecognized('Microphone', I2PTime.getTimeStamp(), sentenceRecognized, 0.0)
                        sentenceRecognized=''
                    except LookupError:
                        print("Could not recognize the speech")

                    print("Ended")"""
			

if __name__ == "__main__":
    #sr_client = ThriftTools.ThriftClient('155.69.52.73', Inputs.constants.DEFAULT_SPEECHRECOGNITION_SERVICE_PORT, SR_Service, 'Speech Recognition')
    sr_client = ThriftTools.ThriftClient('localhost', 12011, SR_Service, 'Speech Recognition')
    try:
        print("AAAAAAAAAAAA")
        sr_client.connect()
        #break
    except TSocket.TTransportException:
        print 'can not connect to Speech Recognition service'
        time.sleep(1)
    except:
        pass
    feedbackNadine_handler = SpeechRecoHandler()
    feedbackNadine_server = ThriftTools.ThriftServerThread(12010,Feedback_Service, feedbackNadine_handler,'Feedback server','localhost')

    thread.start_new_thread(recordAfterFeedback,())

    feedbackNadine_server.start()
    #recordSpeech()
    
