#This program is a test file for speech recognition server 

import sys
import time

#sys.path.append("../")
sys.path.append("../gen-py")
sys.path.append("../i2p/tools/py")

import Inputs.SpeechRecognitionService as SR_Service
from Inputs.ttypes import *
import Inputs.constants

from I2P.ttypes import *

#to ease client server creation in python
import ThriftTools

#time
import I2PTime
import transformations

        
#the function listening for speech inputs        
class SpeechRecoHandler:
    def __init__(self):
        return
        
    def __del__(self):
        return
        
    def sentenceRecognized(self, sensorID, timestamp, sentenceRecognized, confidence):
        print sentenceRecognized

        
if __name__ == "__main__":
    #starting speech recognition service
    speechReco_handler = SpeechRecoHandler()
    #speechReco_server = ThriftTools.ThriftServerThread(Inputs.constants.DEFAULT_SPEECHRECOGNITION_SERVICE_PORT,SR_Service, speechReco_handler,'Speech recognition server','localhost')
    speechReco_server = ThriftTools.ThriftServerThread(12010,SR_Service, speechReco_handler,'Speech recognition server','localhost')
    speechReco_server.start()      
