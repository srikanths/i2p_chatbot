From gen-cpp, 
copy:
UserStorage.h
UserStorage.cpp
UserStorage_types.h
UserStorage_types.cpp

copy the skeleton the FIRST time UserStorage_server.skeleton.cpp
and rename to UserStorage_server.cpp
then you will put your code inside, so any new update of the
thrift interface will generate a new skeleton file that you will
MERGE with your already existing "UserStorage_server.cpp" version,
 else you Will lose everything!