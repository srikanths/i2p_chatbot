# Interface between the Devices and Fusion-World Model

# The devices do not expect any return from the fusion engine
# They are just pushing events

include "I2P.thrift"

namespace cpp imi
namespace java imi
namespace php imi

enum BodySensor
{
    HEAD = 0,
    HANDRIGHT,
    HANDLEFT,
    FOOTRIGHT,
    FOOTLEFT
}

enum Gesture
{
    RAISINGHAND = 0,
    WAVINGHAND,
    SITTING,
    STANDING,
    SHAKINGHAND,
	LOOKINGUP,
	BOWING,
	TURNINGLEFT,
	TURNINGRIGHT,
    ROCK,
    PAPER,
    SCISSOR
#...
}

enum SoundClass
{
    SPEECH = 0,
    IMPULSIVE_SOUND,
    NON_IMPULSIVE_SOUND
#...
}

struct AudioLocalization
{
    1: double azimuth, #angle in radians (0 means in the orientation of the device)
    2: double elevation, #provide only by Nao. Angle in radians (0 means in the orientation of the device)
    3: double intensity, #???
    4: double confidence, #0 to 1, 1 is high confidence
    5: SoundClass soundtype
    #5: doube distance #cannot be provided yet
}

################### V1 ###################
const i32 DEFAULT_SOUND_SERVICE_PORT = 12000;
service SoundService
{
    #For now the source is in polar coordinates, and the elevation angle (polar angle)
    #and distance are not provided by the system
    oneway void sound( 1:string sensorID,
                       2:I2P.Microseconds timestamp,
                       3:AudioLocalization source)
}

const i32 DEFAULT_TRACKING_SERVICE_PORT = 12001;
service TrackingService
{
    oneway void objectMovedDelta( 1: string sensorID,
                                  2: I2P.Microseconds timestamp,
                                  3: I2P.ObjectName movingObject,
                                  4: I2P.Vec3 positionChange,
                                  5: I2P.Vec4 orientationChange,
                                  6: I2P.Microseconds motionDuration)
    # Current object position
    oneway void objectMoved( 1: string sensorID,
                             2: I2P.Microseconds timestamp,
                             3: I2P.ObjectName movingObject,
                             4: I2P.Vec3 position,
                             5: I2P.Vec4 orientation)

}

################### V2 ###################
const i32 DEFAULT_FACE_SERVICE_PORT = 12002;
service FaceService
{
    #the position is the 2D position in the image space
    oneway void faceRecognized( 1:string sensorID,
                                            2:I2P.Microseconds timestamp,
                                            3:string name,
                                            4:I2P.Vec2 position)
    #It is assumed that emotion tracking is performed on same camera
    #than face identification so the agent name should be known
    oneway void emotionRecognized( 1:string sensorID,
                                            2:I2P.Microseconds timestamp,
                                            3:I2P.Emotion emotion,
                                            4:string name)
}

const i32 DEFAULT_USERTRACKING_SERVICE_PORT = 12003;
service UserTrackingService
{
    #The moving status is send only when the position of user
    #significantly change in the camera space
    #The id represent the user for a specific sensor.
    #Every agent is identified by unique combinations of sensorID-userID
    oneway void userNew( 1:string sensorID,
                                2:I2P.Microseconds timestamp,
                                3:i32 userID)
    oneway void userLost( 1:string sensorID,
                                2:I2P.Microseconds timestamp,
                                3:i32 userID)
    #The tracking is done by a kinect camera
    oneway void userSkeletonChanged( 1:string sensorID,
                                                2:I2P.Microseconds timestamp,
                                                3:i32 userID,
                                                4:I2P.Skeleton position)
    #The tracking is done by a normal camera using the 2D image
    oneway void userImageChanged( 1:string sensorID,
                                                2:I2P.Microseconds timestamp,
                                                3:i32 userID,
                                                4:I2P.Vec2 position)

    #It is assumed that gesture recognition is performed on a camera that would do user tracking
    #therefore the userID is the one already used for the tracking
    oneway void gestureStart( 1:string sensorID,
                                        2:I2P.Microseconds timestamp,
                                        3:i32 userID,
                                        4:Gesture gesture)
    oneway void gestureStop( 1:string sensorID,
                                        2:I2P.Microseconds timestamp,
                                        3:i32 userID,
                                        4:Gesture gesture)
}


const i32 DEFAULT_EMOTIONENGINE_SERVICE_PORT = 12004;
service EmotionEngineService
{
    oneway void emotion(1:I2P.Microseconds timestamp,
                        2:I2P.Emotion emotion,
                        3:double intensity)
}

const i32 DEFAULT_TOUCH_SERVICE_PORT = 12005;
service TouchService
{
    oneway void touchStart( 1:string sensorID,
                                2:I2P.Microseconds timestamp,
                                3:I2P.Vec2 position)
    oneway void touchStop( 1:string sensorID,
                                2:I2P.Microseconds timestamp,
                                3:I2P.Vec2 position)
#   oneway void touchBodyStart(1:string sensorID, 2:I2P.Microseconds timestamp, 3:BodySensor bodyPart)
#   oneway void touchBodyStop(1:string sensorID, 2:I2P.Microseconds timestamp, 3:BodySensor bodyPart)
}

const i32 DEFAULT_SPEECHRECOGNITION_SERVICE_PORT = 12006;
service SpeechRecognitionService
{
    oneway void sentenceRecognized( 1:string sensorID,
                                        2:I2P.Microseconds timestamp,
                                        3:string sentence,
                                        4:i32 confidence)
}