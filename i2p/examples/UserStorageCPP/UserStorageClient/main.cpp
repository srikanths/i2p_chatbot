//Thrift Client
#include <transport/TSocket.h>
#include <transport/TBufferTransports.h>
#include <protocol/TBinaryProtocol.h>

#include <iostream>

using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

#include "UserStorage.h"

int main(int argc, char* argv[])
{
    boost::shared_ptr<TSocket> socket(new TSocket("localhost", 9090));
    boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
    boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

    UserStorageClient client = UserStorageClient(protocol);
    
    transport->open();
    
    UserProfile up = UserProfile();
    up.uid = 1;
    up.name = std::string("test User");
    up.blurb = std::string("Thrift is great");

    client.store(up);

    UserProfile up2 = UserProfile();
    client.retrieve(up2,1);

    std::cout << "retrieved " << up2.uid << " " << up.name << " " << up.blurb << std::endl;

    transport->close();
    return 0;
}