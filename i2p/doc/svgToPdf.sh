#!/bin/sh
for file in *.svg;
do inkscape $file -z --export-dpi=500 --export-area-drawing --export-pdf="${file%.svg}.pdf";
done
