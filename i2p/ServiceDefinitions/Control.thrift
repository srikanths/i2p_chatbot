include "I2P.thrift"

namespace cpp imi
namespace java imi
namespace php imi

enum DirectedEmotion
{
    HAPPY_FOR,
    SORRY_FOR,
    RESENTMENT,
    GLOATING,
    GRATITUDE,
    ANGER,
    ADMIRATION,
    REPROACH
}

enum Animation
{
    NOD_YES,
    NOD_NO,
    DONT_KNOW,
    WAVE_HAND,
    SHAKE_HANDS,
    STAND_UP,
    SIT_DOWN,
    GRAB_OBJECT,
    RELEASE_OBJECT,
	ROCK,
	PAPER,
	SCISSOR,
	RIGHT_SWEEP,
	POINTING_YOU,
	WHY,
	HAND_ON_HIP,
	OFFER,
	RIGHT_STRONG_SWEEP
}

enum Status
{
    SUCCESS = 0,
    METHOD_FAILED,
    METHOD_FAILED_IMPOSSIBLE,  # last called method will never work from this client
    METHOD_FAILED_IMPOSSIBLE_IN_CURRENT_STATE, # last called method failed due to current state (e.g. sitting vs. standing)
    METHOD_NOT_YET_IMPLEMENTED, 
}

const map< string, i32 > DEFAULT_CHARACTER_CONTROL_PORT = 
{ 
    'Aurore' : 10000,
    'Brad'   : 10001,
    'Nao'    : 10013,
    #...
    'Zoe'    : 10025,
}

service AgentControl
{
    ##########################################################################
    ## Actions
    #
    # Actions are high-level, semantic requests for behavior given to the agent.
    # 
    # Actions generally return a status indicator.  The call should return
    # almost immediately.  If the call fails, the status should indicate if
    # the action could work again in the future or in some different state.
    
    #  V1
    ##########################################################################
    # The agent looks at the target, possibly turning if needed.
    Status lookAtPosition(     1: I2P.WorldPosition position  );
    Status lookAtTarget(       1: I2P.TargetName target  );
    oneway void endLookAt();
    
    # The agent draws attention to the target, e.g., by directing an 
    # end-effector toward the target.
    Status pointAtPosition(    1: I2P.WorldPosition position  );
    Status pointAtTarget(      1: I2P.TargetName target  );
    oneway void endPointAt();
    
    # The given english phrase is spoken aloud at the specified
    # volume.  Volume should be between 0 (imperceptible) and 1 (loudest).
    Status speak(                1: string phrase,
                                 2: double volume );
                       
    # The agent displays a greeting toward target.  The display
    # might be a hand and arm wave, a bow or other such greeting
    # seen when two people meet for the first time.
    Status greetPosition(      1: I2P.WorldPosition position  );
    Status greetTarget(        1: I2P.TargetName target  );
  
    # The agent plays an open-loop animation which lasts a finite time. 
    # All the animations listed in this file are not necessarily implemented on
    # every agent.
    Status playAnimation( 1: Animation animation );
  
    #  V2
    ##########################################################################
    # The agent should move to the given target.  If the target
    # is a person, the agent should position itself for natural
    # interaction and so should be facing toward the face of the target.
    # Clients can query if the motion is finished by calling isAgentMoving
    Status moveDistance(       1: I2P.Vec3 distance);#delta distance in meters
    Status moveToPosition(     1: I2P.WorldPosition position );
    Status moveToTarget(       1: I2P.TargetName target );
    bool   isAgentMoving();
    
    # Indicate to stop moving
    Status endMove();
    
    # The agent should use an hand or similar end-effector to touch the given
    # target.  Note that orientation of the touch cannot be specified
    # here, so callers may wish to position the agent using moveTo()
    # or lookAt() prior to calling touch().
    Status touchPosition(      1: I2P.WorldPosition position  );
    Status touchTarget(        1: I2P.TargetName target  );
    
    #  V3
    ##########################################################################
    # Use a manipulator to grab the target. Note that orientation 
    # of the touch cannot be specified here, so callers may wish 
    # to position the agent using moveTo()
    Status graspTarget(        1: I2P.TargetName target  );
    
    # End grasp stops the grasp, either aborting it before the object
    # is grasped, or (if the object is already grasped) releasing the object.
    oneway void endGrasp();
    
    #  Beta
    ##########################################################################
    Status shakeHands(           1: I2P.TargetName target  );
    Status sitAtPosition(        1: I2P.WorldPosition position  );
    Status sitOnTarget(          1: I2P.TargetName target  );
    
    # Generally analoguous to "stand"
    Status endSit();
    
    # Short, nearly passive acknowledgement of recognition or agreement,
    # E.g., a quick head nod or small smile.
    Status acknowledgePosition( 1: I2P.WorldPosition position  );
    Status acknowledgeTarget(   1: I2P.TargetName target  );
    
    # Indicate that the target should come to the agent.  Possibly implemented
    # as a hand wave or "come here" wave
    Status beckonPosition(     1: I2P.WorldPosition position  );
    Status beckonTarget(     1: I2P.TargetName target  );
    
    
    ##########################################################################
    ## Affectation
    
    # Function do directly control the facial expression of the entity
    # For now it creates a double uses with the setEmotionalState
    # It is for the purpose to see which solution is more adapted through
    # usage
    Status setFaceExpression( 1: I2P.Facial_Expression newExpression,
                              2: double intensity );
    
    
    # Only one emotional state is possible at a time.
    # Each call specifies the emotional state until the next call 
    # to setEmotionalState or setDirectedEmotionalState
    Status setEmotionalState( 1: I2P.Emotion newEmotion, 
                              2: double intensity );
                              
    # Similar to setEmotionalState, but specifies an emotion directed at
    # a specific person.
    # Note that clients may opt to ignore the target of the emotion,
    # or they may elect to directly express the emotion, e.g., by 
    # looking at directly the target.
    Status setEmotionalStateTowardTarget( 1: DirectedEmotion newDirectedEmotion, 
                                          2: double intensity,
                                          3: I2P.TargetName target );
    
    # Mood is orthogonal to the emotional state, and so the mood is
    # not normally affected by subsequent calls to set*EmotionalState.
    # Mood may affect the execution of subsequent actions, e.g., with 
    # high arousal, moveTo() may result in a faster walk.
    Status setMood( 1: double arousal, 
                    2: double valence, 
                    3: double dominance );
}


