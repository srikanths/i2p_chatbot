###############################################################################
# Find FaceTrackLib
#
# This sets the following variables:
# FaceTrackLib_FOUND - True if FaceTrackLib was found.
# FaceTrackLib_INCLUDE_DIRS - Directories containing the FaceTrackLib include files.
# FaceTrackLib_LIBRARIES - Libraries needed to use FaceTrackLib.


# OVERRIDE to use the Program Files env dir
set(PROGRAMFILES_ "$ENV{PROGRAMFILES}")
message(STATUS "Program Files ${PROGRAMFILES_}")
#Hopefully FaceTrackLib_DIR is set up by kinect installer

#add a hint so that it can find it without the pkg-config
find_path(FaceTrackLib_INCLUDE_DIR FaceTrackLib.h
    HINTS "$ENV{FTSDK_DIR}/inc"
    "${PROGRAMFILES_}/Microsoft SDKs/Kinect/Developer Toolkit v1.5.0/inc"
    PATH_SUFFIXES FaceTrackLib)
#add a hint so that it can find it without the pkg-config
find_library(FaceTrackLib_LIBRARY 
    NAMES FaceTrackLib.lib
    HINTS "$ENV{FTSDK_DIR}/lib/x86" "${PROGRAMFILES_}/Microsoft SDKs/Kinect/Developer Toolkit v1.5.0/lib/x86")

set(FaceTrackLib_INCLUDE_DIRS ${FaceTrackLib_INCLUDE_DIR})
set(FaceTrackLib_LIBRARIES ${FaceTrackLib_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FaceTrackLib DEFAULT_MSG
    FaceTrackLib_LIBRARY FaceTrackLib_INCLUDE_DIR)
    
mark_as_advanced(FaceTrackLib_LIBRARY FaceTrackLib_INCLUDE_DIR)
if(FaceTrackLib_FOUND)
  include_directories(${FaceTrackLib_INCLUDE_DIRS})
  message(STATUS "FaceTrackLib found (include: ${FaceTrackLib_INCLUDE_DIR}, lib: ${FaceTrackLib_LIBRARY})")
endif(FaceTrackLib_FOUND)