# Interface to control the devices from Fusion or AI

include "I2P.thrift"

namespace cpp imi
namespace java imi
namespace php imi

service SpeechCommandService 
{
    #The list contains pairs of expected sentences
    oneway void sendExpectedSentencesMessage(1:list<string> listSentences)
}

const i32 DEFAULT_PICTURE_SERVICE_PORT = 13001;
service PictureService 
{
    #get a jpeg file as a binary blob
    binary takePicture()
}