import sys
sys.path.append("gen-py")

import UserStorage.UserStorage as us
from UserStorage.ttypes import *

from thrift.transport import TTransport
from thrift.transport import TSocket
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer


#class UserStorageHandler(us.Iface):
class UserStorageHandler:
        def __init__(self):
                self.listuser = dict()
                return
        def __del__(self):
                return
        def store(self, user):
                print 'store'
                self.listuser[user.uid] = user
        def retrieve(self, uid):
                print 'retreive'
                return self.listuser[uid]

if __name__ == "__main__":
        hostID = 'localhost'
        #hostID = 'ip address of server'
        portNum = 9090
        handler = UserStorageHandler()
        processor = us.Processor(handler)
        #Python doc explains that python socket (used by TSockect) will resolve the
        #host num depending on dns or local configuration of eth
        #therefore you do not know if it will be ipv6 or ipv4
        #only way to be sure is to force ip address
        transport = TSocket.TServerSocket(host=hostID,port=portNum)
        tfactory = TTransport.TBufferedTransportFactory()
        pfactory = TBinaryProtocol.TBinaryProtocolFactory()
        
        #~ server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
        server = TServer.TThreadedServer(processor, transport, tfactory, pfactory)
        
        print 'Starting python server...'
        server.serve()
        print 'done!'




