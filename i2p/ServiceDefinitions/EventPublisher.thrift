include "I2P.thrift"

namespace cpp imi
namespace java imi
namespace php imi

struct Location
{
    1: I2P.WorldPosition location;     # World coordinates, Z-up
    2: I2P.Quaternion    orientation;
    
    /// If the location is unknown, set to true
    /// note that optional fields will not be 
    /// serialized and sent over the network
    /// so there is no overhead in the ususual case. 
    3: optional bool isUnknown = false;
}

################################################
#  Event Superclass (contained in subclasses as .event)
struct Event
{
    1: I2P.Sequence         sequence,
    2: I2P.Microseconds     startTime,   #< The time the event was first noticed
    3: I2P.Microseconds     finishTime   #< The time the event ended
}

################################################
# Concrete Event classes 
struct PersonEvent
{
    1: Event           event,
    2: I2P.PersonName  actor,           #< Always unique, may be updated
    3: Location        actorLocation
}

struct SoundEvent
{
    1: Event     event,
    2: Location  soundLocation,
    3: double    intensity
}

#    V2 & 3
#struct SpeechEvent
#{
#    1: Event      event,
#    2: I2P.PersonName speakingPerson,
#    3: Location   speakingPersonLocation,
#    4: string     content,
#    5: I2P.PersonName listeningPerson,
#    6: Location   listeningPersonLocation
#}
#
#struct GestureEvent
#{
#    1: Event       event,
#    2: I2P.PersonName  gesturePerson,
#    3: Location    gestureLocation,
#    4: string      gestureType  # Enum?  RaiseHand, PickUpPhone
#}

exception TargetNotFoundException
{
    1: string targetName,
    2: string message
}

####################################################
# Subscription Service
const i32 DEFAULT_EVENT_SUBSCRIPTION_PORT = 11000;
service EventSubscription
{
    # Registers for all world update events to be sent to the
    # given subscriber
    oneway void subscribe( 1: string subscriberAddr,
                           2: i32    subscriberPort );
}

####################################################
# Publisher Service
#
# These methods are called to update clients (AI models) of changes to the world model.
const i32 DEFAULT_WORLD_EVENT_PORT = 11001;
service WorldEvent
{
    oneway void participantEnteredEvent( 1: PersonEvent event );
    oneway void participantLeftEvent(    1: PersonEvent event );
    oneway void participantMovedEvent(   1: PersonEvent event );
    oneway void participantNamedEvent(   1: PersonEvent event, 
                                         2: I2P.PersonName  previousPersonName );
    
    oneway void soundStartedEvent( 1: SoundEvent event );
    oneway void soundFinishedEvent( 1: SoundEvent event );

#    V2 & 3
#    oneway void participantChangedAffectEvent( 1: AffectEvent affectEvent );
#
#    oneway void speechEvent( 1: SpeechEvent event );
#    oneway void gestureEvent( 1: GestureEvent event );
#
#    oneway objectAddedEvent( 1: ObjectEvent event );
#    oneway objectRemovedEvent( 1: ObjectEvent event );
#    oneway objectMovedEvent( 1: ObjectEvent event );

}

const i32 DEFAULT_WORLD_QUERY_PORT = 11002;
service WorldQuery
{
    # Returns the position of the given target
    # TargetName may reference any previously used PersonName or ObjectName
    Location getLocation( 1: I2P.TargetName object ) 
    throws ( 1: TargetNotFoundException exc );
}

