///
/// @file ThriftTools_Win32.cpp
/// @brief Implementation of platform-dependent functions dealing with 
///        networking and thrift.
//------------------------------------------------------------------------------
// Copyright 2012
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------


#include "ThriftTools.hpp"



void imi::initializeThrift( void )
{
    /** Init windows sockets :D **/
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0)
    {
	    /* Tell the user that we could not find a usable */
	    /* Winsock DLL.                                  */
	    printf("WSAStartup failed with error: %d\n", err);

	    // TODO Throw exception??
    }
    /** End init windows socket**/
}


void imi::ignoreSignals( void )
{
    // According to http://tangentsoft.net/wskfaq/articles/bsd-compatibility.html
    // I believe there is no need to handle signals in Win32,
    // so this function does nothing.
}