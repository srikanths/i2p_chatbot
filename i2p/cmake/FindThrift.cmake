# - Try to find Thrift
# Once done this will define
#  THRIFT_FOUND - System has Thrift
#  THRIFT_DIR - The thrift directory
#  THRIFT_INCLUDE_DIRS - The Thrift include directories
#  THRIFT_LIBRARY_DEBUG - The libraries needed to use Thrift
#  THRIFT_LIBRARY_NB_DEBUG - The libraries needed to use Thrift
#  THRIFT_LIBRARIES_DEBUG - packing all debug
#  THRIFT_LIBRARY_RELEASE - The libraries needed to use Thrift
#  THRIFT_LIBRARY_NB_RELEASE - The libraries needed to use Thrift
#  THRIFT_LIBRARIES_RELEASE - packing all release

# Avoid reliance on IMI_LIBRARIES env var for macs
IF(APPLE)
	find_path(THRIFT_INCLUDE_DIR Thrift.h PATHS
		/usr/local/include/thrift
		/opt/local/include/thrift
	)
	set( COMMON_LIB_PATHS /usr/local/lib /opt/local/lib )
	find_library( THRIFT_LIB NAMES thrift PATHS ${COMMON_LIB_PATHS} )
	find_library( THRIFT_LIB NAMES thriftnb PATHS ${COMMON_LIB_PATHS} )
	find_library( EVENT_LIB NAMES event PATHS ${COMMON_LIB_PATHS} )
	set(THRIFT_LIBS ${THRIFT_LIB} ${EVENT_LIB})
	set(THRIFT_FOUND TRUE)
	message(STATUS "Found thrift: ${THRIFT_LIBS}")

	mark_as_advanced(
	  THRIFT_LIB
	  THRIFT_NB_LIB
	  THRIFT_INCLUDE_DIR
	  )
ENDIF(APPLE)

IF(NOT THRIFT_FOUND)
	SET(THRIFT_DIR "$ENV{IMI_LIBRARIES}/thrift")
	IF( NOT IS_DIRECTORY ${THRIFT_DIR})
	    SET(THRIFT_FOUND FALSE)
		MESSAGE( "Thrift not found!  Looking in ${THRIFT_DIR}")
		MESSAGE( "IMI_LIBRARIES env var: $ENV{IMI_LIBRARIES}")
		MESSAGE( "(Note: If using the CMake Windows GUI, you may need to restart it to pick up new ENV vars.)")
	ELSE()
		find_path(THRIFT_INCLUDE_DIR thrift/Thrift.h Thrift.h
			HINTS ${THRIFT_DIR}/include)

		find_library(THRIFT_DEBUG_LIB libthrift
			HINTS ${THRIFT_DIR}/lib/Debug )
		find_library(THRIFT_DEBUG_LIB_NB libthriftnb
			HINTS ${THRIFT_DIR}/lib/Debug )
			
		find_library(THRIFT_RELEASE_LIB libthrift
			HINTS ${THRIFT_DIR}/lib/Release )
		find_library(THRIFT_RELEASE_LIB_NB libthriftnb
			HINTS ${THRIFT_DIR}/lib/Release )

		set(THRIFT_INCLUDE_DIRS ${THRIFT_INCLUDE_DIR} )
		set(THRIFT_LIBRARY_DEBUG ${THRIFT_DEBUG_LIB} )
		set(THRIFT_LIBRARY_NB_DEBUG ${THRIFT_DEBUG_LIB_NB} )
		set(THRIFT_LIBRARY_RELEASE ${THRIFT_RELEASE_LIB} )
		set(THRIFT_LIBRARY_NB_RELEASE ${THRIFT_RELEASE_LIB_NB} )
		set(THRIFT_LIBRARIES_DEBUG ${THRIFT_DEBUG_LIB} ${THRIFT_DEBUG_LIB_NB} )
		set(THRIFT_LIBRARIES_RELEASE ${THRIFT_RELEASE_LIB} ${THRIFT_RELEASE_LIB_NB} )
		
		include(FindPackageHandleStandardArgs)
		# handle the QUIETLY and REQUIRED arguments and set THRIFT_FOUND to TRUE
		# if all listed variables are TRUE
		find_package_handle_standard_args(Thrift  DEFAULT_MSG
			THRIFT_DEBUG_LIB THRIFT_DEBUG_LIB_NB THRIFT_RELEASE_LIB THRIFT_RELEASE_LIB_NB THRIFT_INCLUDE_DIR)

		mark_as_advanced(THRIFT_DEBUG_LIB THRIFT_DEBUG_LIB_NB THRIFT_RELEASE_LIB THRIFT_RELEASE_LIB_NB THRIFT_INCLUDE_DIR)
	ENDIF()
ENDIF(NOT THRIFT_FOUND)

