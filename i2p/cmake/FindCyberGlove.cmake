# - Try to find CyberGlove
# Once done this will define
#  CyberGlove_FOUND - System has CyberGlove
#  CyberGlove_INCLUDE_DIRS - The CyberGlove include directories
#  CyberGlove_LIBRARY_DEBUG - The libraries needed to use CyberGlove
#  CyberGlove_LIBRARY_RELEASE - The libraries needed to use CyberGlove
#  CyberGlove_LIBRARY_DIR - The libraries needed to use CyberGlove


set(CyberGlove_INCLUDE_DIR "C:/Program Files (x86)/CyberGlove Systems/VirtualHand SDK/include/vhandtk"
 "C:/Program Files (x86)/CyberGlove Systems/VirtualHand SDK/include/vtidm"
 "C:/Program Files/CyberGlove Systems/VirtualHand SDK/include/vhandtk"
 "C:/Program Files/CyberGlove Systems/VirtualHand SDK/include/vtidm" )
 
 find_library(CyberGlove_LIBRARY_REGISTERY 
    NAMES CGS_VirtualHandRegistry.lib
    HINTS "C:/Program Files/CyberGlove Systems/VirtualHand SDK/lib/winnt_386/Release/"
    HINTS "C:/Program Files (x86)/CyberGlove Systems/VirtualHand SDK/lib/winnt_386/Release/")
	
find_library(CyberGlove_LIBRARY_DEVICE 
    NAMES CGS_VirtualHandDevice.lib
    HINTS "C:/Program Files/CyberGlove Systems/VirtualHand SDK/lib/winnt_386/Release/"
    HINTS "C:/Program Files (x86)/CyberGlove Systems/VirtualHand SDK/lib/winnt_386/Release/")
	
set(CyberGlove_LIB_DEBUG ${CyberGlove_LIBRARY_REGISTERY} ${CyberGlove_LIBRARY_DEVICE} )
set(CyberGlove_LIB_RELEASE ${CyberGlove_LIBRARY_REGISTERY} ${CyberGlove_LIBRARY_DEVICE})
set(CyberGlove_LIB_DIR "C:/Program Files (x86)/CyberGlove Systems/VirtualHand SDK/lib/winnt_386/Release"
"C:/Program Files/CyberGlove Systems/VirtualHand SDK/lib/winnt_386/Release" )
set(CyberGlove_LIBRARY_DIRS ${CyberGlove_LIB_DIR})

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LibSVM_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(CyberGlove DEFAULT_MSG CyberGlove_INCLUDE_DIR CyberGlove_LIB_DEBUG CyberGlove_LIB_RELEASE CyberGlove_LIB_DIR)

mark_as_advanced(CyberGlove_INCLUDE_DIR CyberGlove_LIB_DEBUG CyberGlove_LIB_RELEASE CyberGlove_LIB_DIR)