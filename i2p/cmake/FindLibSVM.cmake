# - Try to find LibSVM
# Once done this will define
#  LibSVM_FOUND - System has LibSVM
#  LibSVM_INCLUDE_DIRS - The LibSVM include directories
#  LibSVM_LIBRARY_DEBUG - The libraries needed to use LibSVM
#  LibSVM_LIBRARY_RELEASE - The libraries needed to use LibSVM
#  LibSVM_LIBRARY_DIR - The libraries needed to use LibSVM

find_path(LibSVM_INCLUDE_DIR svm.h
    HINTS $ENV{IMI_LIBRARIES}
    PATH_SUFFIXES libsvm-3.12)

find_library(LibSVM_LIB_DEBUG libsvm
    HINTS $ENV{IMI_LIBRARIES}
    PATH_SUFFIXES libsvm-3.12/lib/debug)
    
find_library(LibSVM_LIB_RELEASE libsvm
    HINTS $ENV{IMI_LIBRARIES}
    PATH_SUFFIXES libsvm-3.12/lib/release)
    
find_path(LibSVM_LIB_DIR Debug/libsvm.lib Release/libsvm.lib
    HINTS $ENV{IMI_LIBRARIES}
    PATH_SUFFIXES libsvm-3.12/lib/)

set(LibSVM_INCLUDE_DIRS ${LibSVM_INCLUDE_DIR} )
set(LibSVM_LIBRARY_DEBUG ${LibSVM_LIB_DEBUG} )
set(LibSVM_LIBRARY_RELEASE ${LibSVM_LIB_RELEASE} )
set(LibSVM_LIBRARY_DIR ${LibSVM_LIB_DIR} )


include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LibSVM_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LibSVM  DEFAULT_MSG LibSVM_INCLUDE_DIR LibSVM_LIB_DEBUG LibSVM_LIB_RELEASE LibSVM_LIB_DIR)

mark_as_advanced(LibSVM_INCLUDE_DIR LibSVM_LIB_DEBUG LibSVM_LIB_RELEASE LibSVM_LIB_DIR)
