///
/// @file ThriftTools.hpp
/// @brief Common library tools for all i2p projects.
///        Note that implementations may be project-dependent in thier own
///        os-specific .cpp files.  Non-os-specific implementations should be
///        in ThriftTools.cpp.
///
//------------------------------------------------------------------------------
// Copyright 2012
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

#ifndef I2P_WORLD_MODEL_THRIFT_TOOLS_HPP
#define I2P_WORLD_MODEL_THRIFT_TOOLS_HPP

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TJSONProtocol.h>
#include <thrift/protocol/TDebugProtocol.h>
#include <thrift/server/TThreadedServer.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TFileTransport.h>
#include <thrift/transport/TSimpleFileTransport.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/transport/TTransport.h>
#include <fstream>
#include <iostream>
#include <sstream>

#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/program_options.hpp>
#include <boost/thread/locks.hpp>

// Defines for locks.
#define LOCK(x) boost::lock_guard<boost::recursive_mutex> lock((x));

namespace imi
{
    //replace int64_t by imi:Microseconds
    int64_t getTimeStamp();

    /// Ignore any signals that may be raised by networking failures,
    /// allowing those errors to be handled by the local code.
    void ignoreSignals( void );

    /// Do any startup initialization.  Called just after entering main()
    void initializeThrift( void );

    /// Maintain the given server, restarting as needed
    template< typename ServerT >
    void keepAlive( ServerT& server, int port )
    {
        using namespace imi;

        ignoreSignals();
        while( true )
        {
            try
            {
                std::cerr << "Serving on port " << port << ".\n";
                server.serve();
                std::cerr << "Done serving on port " << port << ".\n";
            } 
            catch( std::exception exc ) 
            {
                std::cerr << "Lost Server on port " << port << ". Error: " << exc.what();
            }
            server.stop();
        }
    }

    class TimestampEventHandler : public ::apache::thrift::TProcessorEventHandler
    {
    public:
        TimestampEventHandler( const std::string& logFileName ) 
        {
            LOCK(m_lock);
            m_log.open( logFileName.c_str(), std::ios::out | std::ios::app );
        }
        /// Guaranteed to be at the end of the call
        virtual void preRead( void* ctx, const char* fn_name )
        {
            LOCK(m_lock);
            m_log << getTimeStamp() << '\n';
            m_log.flush();
        }
        virtual ~TimestampEventHandler( ) { m_log.close();  std::cerr<< "\n\n\n\n----------------------------TIMESTAMP DTOR---------------------\n\n\n\n";}
    private:
        mutable boost::recursive_mutex m_lock;
        std::ofstream m_log;
    };

    
    class CountingTimestampEventHandler : public ::apache::thrift::TProcessorEventHandler
    {
    public:
        CountingTimestampEventHandler( const std::string& logFileName )
        {
            m_callCount = 0;
            m_log.open( logFileName.c_str(), std::ios::out | std::ios::app );
        }
        /// Called before other callbacks
        virtual void* getContext(const char* fn_name, void* serverContext )
        {
            m_callCount++;
            int* ctxCount = new int;
            *ctxCount = 0;
            m_log << getTimeStamp() << "\tgetContext:    \t\t" << fn_name << '\t'
            << m_callCount << '\t' << *ctxCount << '\n';
            (*ctxCount)++;
            return (void*)ctxCount;
        }
        virtual void freeContext( void* ctx, const char* fn_name )
        {
            int* ctxCount = (int*)ctx;
            m_log << getTimeStamp() << "\tfreeContext:\t\t" << fn_name << '\t'
            << m_callCount << '\t' << *ctxCount << '\n';
            delete (int*)ctx;
        }
        virtual void asyncComplete(void* ctx, const char* fn_name)
        {
            int* ctxCount = (int*)ctx;
            m_log << getTimeStamp() << "\tasyncComplete:\t\t" << fn_name << '\t'
            << m_callCount << '\t' << *ctxCount << '\n';
            (*ctxCount)++;
        }
        virtual void postWrite( void* ctx, const char* fn_name, uint32_t bytes )
        {
            int* ctxCount = (int*)ctx;
            m_log << getTimeStamp() << "\tpostWrite:   \t\t" << fn_name << '\t'
            << m_callCount << '\t' << *ctxCount << '\n';
            (*ctxCount)++;
        }
        virtual void preRead( void* ctx, const char* fn_name )
        {
            int* ctxCount = (int*)ctx;
            m_log << getTimeStamp() << "\tpreRead:    \t\t" << fn_name << '\t'
            << m_callCount << '\t' << *ctxCount << '\n';
            (*ctxCount)++;
        }
        virtual ~CountingTimestampEventHandler( ) { }
    private:
        mutable boost::recursive_mutex m_lock;
        int m_callCount;
        std::ofstream m_log;
    };

    class TimestampEventHandlerStateless : public ::apache::thrift::TProcessorEventHandler
    {
    public:
        TimestampEventHandlerStateless( const std::string& logFileName ) 
        {
            m_fileName = logFileName;
        }

        /// Called before other callbacks
        virtual void* getContext(const char* fn_name, void* serverContext )
        {
            std::ofstream* logFile = new std::ofstream;
            logFile->open( m_fileName.c_str(), std::ios::out | std::ios::app );
            *logFile << getTimeStamp() << "\tgetContext\t" << fn_name << "\t" << serverContext << "\n";
            return (void*)logFile;
        }

        virtual void freeContext( void* ctx, const char* fn_name )
        {
            logFileFromContext(ctx) << getTimeStamp() << "\tfreeContext: " << fn_name << '\n';
            logFileFromContext(ctx).close();
            delete &logFileFromContext(ctx);
        }

        virtual void asyncComplete(void* ctx, const char* fn_name) 
        {
            logFileFromContext(ctx) << getTimeStamp() << "\tasync: " << fn_name << '\n';
        }

        virtual void postWrite( void* ctx, const char* fn_name, uint32_t bytes )
        {
            logFileFromContext(ctx) << getTimeStamp() << "\tpost: " << fn_name << '\n';
        }

        virtual void preRead( void* ctx, const char* fn_name )
        {
            logFileFromContext(ctx) << getTimeStamp() << "\tpre: " << fn_name << '\n';
        }

        virtual ~TimestampEventHandlerStateless() { }
    private:
        std::ofstream& logFileFromContext( void* ctx )
        {
            return *((std::ofstream*)ctx);
        }
        std::string m_fileName;
    };


    std::string timeLogFilename( const std::string& baseFilename );    
    std::string eventLogFilename( const std::string& baseFilename );

    /// Create and startup the given server
    template<typename HandlerT,typename ProcessorT>
    void createServer(boost::shared_ptr<HandlerT> handler, int port)
    {        
        std::cerr << "createServer(" << port << ")\n";
        using namespace ::apache::thrift;
        using namespace ::apache::thrift::protocol;
        using namespace ::apache::thrift::transport;
        using namespace ::apache::thrift::server;

        boost::shared_ptr<TProcessor> processor(new ProcessorT(handler));
        boost::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
        boost::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
        boost::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

        TThreadedServer server(processor, serverTransport, transportFactory, protocolFactory);
        keepAlive(server, port);
    };

    /// Create and startup the given server, logging all transactions (maybe large)
    /// to a file of the given name.
    template<typename HandlerT,typename ProcessorT>
    void createLoggedServer( boost::shared_ptr<HandlerT> handler, 
                             int port, 
                             const std::string& logFilename)
    {
        std::cerr << "createLoggedServer(" << logFilename << ")\n";
        using namespace ::apache::thrift;
        using namespace ::apache::thrift::protocol;
        using namespace ::apache::thrift::transport;
        using namespace ::apache::thrift::server;

        boost::shared_ptr<TProcessor> processor(new ProcessorT(handler));
        boost::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
        boost::shared_ptr<TProtocolFactory> protocolFactory( new TBinaryProtocolFactory() );
        
        // Setting new event handler for logging timestamps
        boost::shared_ptr<TProcessorEventHandler> eventTimestamp(new TimestampEventHandler( timeLogFilename(logFilename) ) );
        processor->setEventHandler(eventTimestamp);

        boost::shared_ptr<TTransport> logTransport( new TFileTransport( eventLogFilename(logFilename) ) );        
        boost::shared_ptr<TTransportFactory> pipedTransportFactory( new TPipedTransportFactory(logTransport) );

        TThreadedServer server(processor, serverTransport, pipedTransportFactory, protocolFactory);

        keepAlive( server, port );
    }

    /// Create and startup the given server, logging all transactions (maybe large)
    /// to a file of the given name.
    template< typename HandlerT, typename ProcessorT >
    void readLoggedEvents( boost::shared_ptr<HandlerT> handler,                             
    		               const std::string& logFilename )
    {
        std::cerr << "readLoggedEvents(" << logFilename << ")\n";
        using namespace ::apache::thrift;
        using namespace ::apache::thrift::protocol;
        using namespace ::apache::thrift::transport;        

        boost::shared_ptr<TProcessor> processor(new ProcessorT(handler));
        boost::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());
            
        boost::shared_ptr<TFileTransport> logTransport( new TFileTransport( eventLogFilename(logFilename) ) );
            
        boost::shared_ptr<TProtocol> inputProtocol = protocolFactory->getProtocol(logTransport);
        boost::shared_ptr<TProtocol> outputProtocol = protocolFactory->getProtocol(boost::shared_ptr<TNullTransport>( new TNullTransport() ));

        // TFileProcessor fileReader(processor,
        //                           protocolFactory,
        //                           logTransport);
        // std::cerr << "Number of chunks:" << logTransport->getNumChunks() << "\n";
        // fileReader.process(0, true);
            
        int numEvents = 0;

        std::ifstream timeLogger;
        timeLogger.open( timeLogFilename(logFilename).c_str(), std::ios::in );

        int64_t logCurrTime;
        int64_t logPrevTime;

        timeLogger >> logPrevTime;
        timeLogger >> logCurrTime;

        int64_t timeNow = getTimeStamp();
        int64_t timePrev = timeNow;

        int64_t lag = (logCurrTime - logPrevTime);  // microsecs elapsed between first and second events
        bool isRunning = true;
        while( isRunning ) 
        {
            try 
            {
                timeNow = getTimeStamp();
                // busy wait for the time of the next log message
                if ((timeNow - timePrev) < lag) continue;
                // Enough time has elapsed, process next log message

                timePrev = timeNow;
                logPrevTime = logCurrTime;
                timeLogger >> logCurrTime;
                lag = (logCurrTime - logPrevTime);

                processor->process( inputProtocol, outputProtocol, NULL /* Connection context*/ );

                // boost::this_thread::sleep(boost::posix_time::microseconds ((timeStamp - prevTime)));
                // usleep((timeStamp - prevTime));
                // prevTime = timeStamp;
                numEvents ++;
                // std::cerr << "Done event number: " << numEvents << "\n";
                // std::string fname;
                // TMessageType mtype;
                // int32_t seqid;
                // inputProtocol->readMessageBegin(fname, mtype, seqid);
                // if (mtype != T_CALL) {
                //     throw TException("Unexpected message type");
                // }
                // TType ftype;
                // int16_t fid;
                // while (true) {
                //     inputProtocol->readFieldBegin(fname, ftype, fid);
                //     if (ftype == T_STOP) {
                //     break;
                //     }

                //     // Peek at the variable
                //     // peek(in, ftype, fid);
                //     (void) fid;
                //     inputProtocol->skip(ftype);
                //     inputProtocol->readFieldEnd();
                // }
                // inputProtocol->readMessageEnd();
                // inputProtocol->getTransport()->readEnd();
                // numEvents++;
                // std::cerr << "event number: " << numEvents << "\n";
                // if (numEvents == 500) {
                //    std::cerr << "Done processing\n.";
                //    break;
                //}
            } catch (TEOFException& teof) 
            {
                std::cerr << "Finished with log file: \"" << logFilename << "\"\n.";
                std::cerr << "Done event number: " << numEvents << "\n";
                isRunning = false;
            } catch (TException &te) 
            {
                std::cerr << te.what() << "\n";
                isRunning = false;
            }
        }
    }

    /// Create a server that gets events from where ever the program options say-- either a log
    /// or the network.  If from the network, optionally log messages to a file.
    template< typename ServiceT, typename ServiceProcessorT >
    void createEventGeneratingThread( boost::shared_ptr<ServiceT> service,
                                      int port,
                                      boost::program_options::variables_map& vm )
    {
        std::cerr << "Starting Event Generating Thread - ";
        if(vm.count("readSensorEventsFromFile"))
        {
            // reading case
            std::stringstream ss;
            ss << vm["readSensorEventsFromFile"].as<std::string>() << "-" << port ;
            std::cerr << "from file \"" << ss.str() << "\"\n";
            boost::thread trackerDispatcherThread( readLoggedEvents<ServiceT, ServiceProcessorT>,
                                                   service, 
                                                   ss.str() );
            return;
        }
        if(vm.count("writeSensorEventsFromFile"))
        {
            // writing case
            std::stringstream ss;
            ss << vm["writeSensorEventsFromFile"].as<std::string>() << "-" << port ;
            std::cerr << "from file \"" << ss.str() << "\"\n";
            boost::thread trackerDispatcherThread( createLoggedServer<ServiceT, ServiceProcessorT>,
                                                   service, 
                                                   port,
                                                   ss.str() );
            return;
        }   
        // listening-to-network case
        std::cerr << "listening to network messages on port: " << port << "\n";
        boost::thread trackerDispatcherThread( createServer<ServiceT, ServiceProcessorT>,
                                               service, 
                                               port ); 
    }
}

#endif
