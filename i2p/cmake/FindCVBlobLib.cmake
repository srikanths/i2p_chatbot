# - Try to find CVBlobLib
# Once done this will define
#  CVBlobLib_FOUND - System has CVBlobLib
#  CVBlobLib_INCLUDE_DIRS - The CVBlobLib include directories
#  CVBlobLib_LIBRARY_DEBUG - The libraries needed to use CVBlobLib
#  CVBlobLib_LIBRARY_RELEASE - The libraries needed to use CVBlobLib

find_path(CVBlobLib_INCLUDE_DIR blob.h
  HINTS 
  $ENV{IMI_LIBRARIES}
  "/home/imi/Libraries/cvblobs8.3_linux/"
  PATH_SUFFIXES cvblobslib)

if(UNIX)
  find_library(CVBlobLib_LINUX 
    NAMES libblob
    HINTS /home/imi/Libraries/cvblobs8.3_linux/
    )
endif(UNIX)

find_library(CVBlobLib_LIB_DEBUG cvblobslib
  HINTS $ENV{IMI_LIBRARIES}
  PATH_SUFFIXES cvblobslib/Debug)
    
find_library(CVBlobLib_LIB_RELEASE cvblobslib
    HINTS $ENV{IMI_LIBRARIES}
    PATH_SUFFIXES cvblobslib/Release)
    

set(CVBlobLib_INCLUDE_DIRS ${CVBlobLib_INCLUDE_DIR} )

if(WIN32)
  set(CVBlobLib_LIBRARY_DEBUG ${CVBlobLib_LIB_DEBUG} )
  set(CVBlobLib_LIBRARY_RELEASE ${CVBlobLib_LIB_RELEASE} )
  include(FindPackageHandleStandardArgs)
  # handle the QUIETLY and REQUIRED arguments and set LibSVM_FOUND to TRUE
  # if all listed variables are TRUE
  find_package_handle_standard_args(CVBlobLib DEFAULT_MSG CVBlobLib_INCLUDE_DIR CVBlobLib_LIB_DEBUG CVBlobLib_LIB_RELEASE)
  mark_as_advanced(CVBlobLib_INCLUDE_DIR CVBlobLib_LIB_DEBUG CVBlobLib_LIB_RELEASE)
endif(WIN32)

if(UNIX)
  set(CVBlobLib_LIBRARY ${CVBlobLib_LINUX} )
  include(FindPackageHandleStandardArgs)
  # handle the QUIETLY and REQUIRED arguments and set LibSVM_FOUND to TRUE
  # if all listed variables are TRUE
  find_package_handle_standard_args(CVBlobLib DEFAULT_MSG CVBlobLib_INCLUDE_DIR CVBlobLib_LIBRARY)
  mark_as_advanced(CVBlobLib_INCLUDE_DIR CVBlobLib_LIBRARY)
endif(UNIX)


